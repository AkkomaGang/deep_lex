defmodule DeepLex do
  @moduledoc """
  Documentation for `DeepLex`.
  """
  use Tesla

  @type translation :: %{detected_source_language: String.t(), text: String.t()}

  @type response :: %{translations: [translation]}

  plug(Tesla.Middleware.EncodeFormUrlencoded)
  plug(Tesla.Middleware.DecodeJson, engine_opts: [keys: :atoms!])

  defp base_url(:free) do
    "https://api-free.deepl.com/v2/"
  end

  defp base_url(:pro) do
    "https://api.deepl.com/v2/"
  end

  @spec translate(String.t(), String.t(), String.t(), String.t()) ::
          {:ok, response} | {:error, String.t()}
  def translate(api_key, tier, string, to_language) do
    with {:ok, response} <- request(api_key, tier, string, to_language) do
      {:ok, response.body}
    else
      {:error, reason} -> {:error, reason}
    end
  end

  defp request(api_key, tier, string, to_language) do
    post(base_url(tier) <> "translate", %{
      auth_key: api_key,
      text: string,
      target_lang: to_language
    })
  end
end
